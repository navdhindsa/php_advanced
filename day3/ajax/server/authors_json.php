<?php 

  $dbh = new PDO('sqlite:database1.sqlite');
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $query = 'select * from author ';
  $stmt=$dbh->prepare($query);
  $stmt->execute();
  $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
  //if author_id is set, select that author
  if(isset($_GET['author_id'])){
    $author_id=$_GET['author_id'];
  if(isset($author_id)){
    $query = 'select * from author where author_id = :author_id';
    $stmt=$dbh->prepare($query);
    $stmt ->bindValue(':author_id',$author_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
  }
  }
  //else select all authors
  header('Content-type: application/json');
  echo json_encode($result);
