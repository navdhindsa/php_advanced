<?php


function string($value){
	return preg_match('/^([A-z\s\-\']+)$/',$value);
}

if($_SERVER['REQUEST_METHOD']=='POST'){
	$errors=[];

	$validator=[
		'first_name' =>'string',
		'last_name' => 'string',

		'email' => 'email',
		'password' => 'strength',
		'cpassword' => 'match',

	];

	foreach($_POST as $key =>$value){
		if(array_key_exists($key, $validator)){
			$errors[$key]=!call_user_func($validator[$key],$value);
		}
	}
}


header('Content-type: application/json');
echo json_encode($errors);