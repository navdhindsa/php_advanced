<?php

$user = [
  'first_name'=> 'Dave',
  'last_name'=> 'Jones',
  'age'=>'22',
  'email'=>'dave@steve.com',
  'hobbies'=> ['reading','writing','arithmetic']
];

header('Content-type: application/json');
echo json_encode($user);