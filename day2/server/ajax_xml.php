<?php


require 'ArrayToXml.php';

$user = [
  'first_name'=> 'Dave',
  'last_name'=> 'Jones',
  'age'=>'22',
  'email'=>'dave@steve.com',
  'hobbies'=> ['reading','writing','arithmetic']
];

$obj = new ArrayToXml($user, 'user');


$xml = $obj->toXml();
header('Content-type: application/xml');
echo $xml;