<!DOCTYPE html>
<html>
<head lang="en">
  <meta charset="utf-8">
  <title>Assignment 1</title>
  <script>
    
    //creating new XMLHttpRequest object
    var xhr=new XMLHttpRequest();
    
    //function on the changing state of xhr
    xhr.onreadystatechange=function(){
      
      //if the ready state change is 4, then do the following
      if(xhr.readyState==4){
        
        //parse the data which is json formatted string and convert into an object
        var data = JSON.parse(xhr.responseText);
        
        //calling the function to make the list view passing data as a parameter
        buildLinks(data);
      }//if ends
    }//onreadystatechange function ends
    
    // function to build get the list view 
    function buildLinks(data){
    var html='<ul>\n';
    //loop to view the list
    for(var i=0;i<data.length;i++){
    //get the id and title
    var id= data[i].book_id;
    var name= data[i].title;
      //put that data in html
    html+= '<li data-id='+id+'> '+name+ '</li>\n';
    }
    html+='</ul>\n';
    document.getElementById('list').innerHTML=html;
      //call the function to handle the clicks
    handleClicks();
    }
    window.onload=function(){
      //open and send functions
      xhr.open('GET','server/server.php');
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.send();
    }
    
    //function to handle the clicks, when any li item is clicked
    function handleClicks(){
      //getting the list items
      var li= document.getElementById('list').getElementsByTagName('li');
      //get the value of data-id attribute and call function to show the details
      for(var i=0;i<li.length;i++){
        //on click get the data-id attribute and call the function to show the detail of page
        li[i].onclick=function(e){
          var id=e.target.getAttribute('data-id');
          showDetail(id);
          //add current class to the link that is clicked
          var current= document.getElementsByClassName('current');
          if(current.length>0){
            current[0].className=current[0].className.replace('current','');
          }
          this.className+='current';
        }
      }
    }
    
    //function to build details, adding all the properties in the html
    function buildDetail(data){
      var html= '<h2>'+data.title+'</h2>';
      html+='<img src="images/'+data.image+'" alt="'+data.title+'" />';
      html +='<p><span class="first">Title:</span> '+data.title + '</p>' ; 
      html +='<p><span class="first">Number of Pages: </span>'+data.num + '</p>' ; 
      html +='<p><span class="first">Year Published: </span>'+data.year + '</p>' ; 
      html +='<p><span class="first">In print: </span>'+data.in_print + '</p>' ; 
      html +='<p><span class="first">Author: </span>'+data.author + '</p>' ; 
      html +='<p><span class="first">Publisher: </span>'+data.publisher + '</p>' ; 
      html +='<p><span class="first">Format: </span>'+data.format + '</p>' ; 
      html +='<p><span class="first">Genre: </span>'+data.genre + '</p>' ; 
      document.getElementById('detail').innerHTML=html;
    }
    //function to show the details and getting the data from the server using the book_id
    function showDetail(id){   	      
      xhr.onreadystatechange=function(){
        if(xhr.readyState==4){
          var data = JSON.parse(xhr.response);
          buildDetail(data);
        }
      }
      var txt='server/server.php?book_id='+id;
      //sending the request and open
      xhr.open('GET',txt);
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
      xhr.send();
    }
  </script>
  <link rel="stylesheet" type="text/css" href="styles/styles.css" />
  <style>
    div{
      box-sizing: border-box;
    }
    #list,#detail{
      width: 50%;
      float: left;
    }
  </style>
</head>
<body>
  <h1>Vanilla JavaScript</h1>
  <!--Links to various pages-->
  <p class="link" ><a href="books_jquery.php" >jQuery</a></p>
  <p class="link" ><a href="books_json.php" id="page" >Vanilla JavaScript</a></p>
  <p class="link"><a href="server/server.php" >Server</a></p>
  <!--  output list in html-->
  <div id="list"></div>
  <!--  if single book output detail-->
  <div id="detail"></div>
</body>
</html>