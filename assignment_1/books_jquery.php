<!DOCTYPE html>
<html lang="en">
<head>
  <title>Books in jQuery</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
    //document.ready function
    $(document).ready(function(){
      //when no book is selected get all books
      $.get('server/server.php',null,function(response){
        console.log(response);
        //call the listView function to make all the details
        listView(response);
      });
    });
    
    // function to build get the list view
    function listView(data){
      //creating the list of items in the listview
      var content = '<ul>';
      for(var i=0; i<data.length; i++){
        content += '<li data-id="'+data[i].book_id+'">'+ data[i].title + '</li>';
      }
      content += '</ul>';
      $('#list').html(content);
      //adding the current class to make it visible the selected class
      $('#list li').click(function(e){
        var book_id=e.target.getAttribute('data-id');
        $('li').removeClass('current');
        $(this).addClass('current');
        //handling the clicked link
        clickingLinks(book_id);
      });
    }
    
    //getting the id of clicked item and sending it to server to get details
    function clickingLinks(book_id){
      $.get('server/server.php?book_id='+book_id,null,function(response){
        //calling the function to build the detail view
        buildingData(response);
      });
    }
    //building the details and adding to html and loading it in a div
    function buildingData(response){
      var html ='<h2>'+ response.title +'</h2>';
      html+='<img src="images/'+response.image+'" alt="'+response.title+'" />';
      html +='<p><span class="first">Title: </span>'+response.title + '</p>' ; 
      html +='<p><span class="first">Number of Pages: </span>'+response.num + '</p>' ; 
      html +='<p><span class="first">Year Published: </span>'+response.year + '</p>' ; 
      html +='<p><span class="first">In print: </span>'+response.in_print + '</p>' ; 
      html +='<p><span class="first">Author: </span>'+response.author + '</p>' ; 
      html +='<p><span class="first">Publisher: </span>'+response.publisher + '</p>' ; 
      html +='<p><span class="first">Format: </span>'+response.format + '</p>' ; 
      html +='<p><span class="first">Genre: </span>'+response.genre + '</p>' ; 
      $('#detail').hide().fadeIn(800).html(html);
    }
  </script>
  <link rel="stylesheet" type="text/css" href="styles/styles.css" />
</head>
<body>
  <h1>jQuery</h1>
  <!--Links to various pages-->
  <p class="link" ><a href="books_jquery.php" title="jQuery page" id="page">jQuery</a></p>
  <p class="link"><a href="books_json.php" >Vanilla JavaScript</a></p>
  <p class="link"><a href="server/server.php" >Server</a></p>
  <!--  output list in html-->
  <div id="list"></div>
  <!--  if single book output detail-->
  <div id="detail"></div>
</body>
</html>