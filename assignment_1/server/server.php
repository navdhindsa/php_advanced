<?php 
  
  //storing all headers in a variable
  $headers=getallheaders();
  
  //check if the request is from ajax by checking the header value
  if(array_key_exists('X-Requested-With',$headers)){
    
    //database credentials
    define('DB_USER','root');
    define('DB_PASS','root');
    define('DB_DSN','mysql:host=localhost;dbname=booksite');
    //creating new PDO object
    $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //if book_id is set, select that book
    if(isset($_GET['book_id'])){
    $book_id=$_GET['book_id'];
    if(isset($book_id)){
      //query to select all details of book
      $query = 'SELECT 
      book.title AS title,
      author.name AS author,
      book.book_id AS book_id,
      genre.name AS genre,
      book.num_pages AS num,
      book.author_id AS author_id,
      book.year_published AS year,
      book.in_print AS in_print,
      book.price AS price,
      book.image AS image,
      publisher.name AS publisher,
      publisher.city AS pub_city,
      author.country AS country,
      format.name AS format,
      publisher.publisher_id AS publisher_id,
      book.description AS description
      FROM book
      JOIN author USING(author_id)
      JOIN publisher USING(publisher_id)
      JOIN genre USING(genre_id)
      JOIN format USING(format_id)
      WHERE book_id=:book_id';
      $stmt =$dbh-> prepare($query);
      $params= array(':book_id'=>$book_id);
      $stmt -> execute($params);
      $result=  $stmt-> fetch(PDO::FETCH_ASSOC);
      }
    }
    else{
      //else select all books
      $query = 'select * from book ';
      $stmt=$dbh->prepare($query);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
    //setting header to tell that content is json
    header('Content-type: application/json');
    //encoding the output in json format
    echo json_encode($result);
  }
  
  //if the request is not from ajax, display errors
  else{
  echo json_encode("You are not using AJAX Steve!! Click to go back to <a href='../books_json.php' >Vanilla JavaScript");
  }