<?php

//author books

function getAuthor($dbh, $author_id)
{
  $query = 'SELECT 
  book.title AS title,
  book.book_id AS book_id,
  author.name AS author,
  book.author_id AS author_id,
  genre.name AS genre,
  book.num_pages AS num,
  book.year_published AS year,
  book.price AS price,
  book.image AS image,
  book.description AS description
  FROM book
  JOIN author USING(author_id)
  JOIN genre USING(genre_id)
  WHERE author_id=:author_id';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':author_id'=>$author_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
 
  
  
  
}