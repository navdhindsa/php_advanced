<?php

//book model

function getBooks($dbh, $num_books)
{
  $query = 'SELECT 
  book.title AS title,
  author.name AS author,
  
  book.book_id AS book_id,
  genre.name AS genre,
  book.num_pages AS num,
  
  book.author_id AS author_id,
  book.year_published AS year,
  book.price AS price,
  book.image AS image,
  book.description AS description
  FROM book
  JOIN author USING(author_id)
  JOIN genre USING(genre_id)';
  
  $stmt =$dbh-> prepare($query);
  
  $stmt -> execute();
  
  $result =  $stmt-> fetchAll(PDO::FETCH_ASSOC);
  $random_keys = array_rand($result,$num_books);
  $random = array();
  
  foreach($random_keys AS $values){
    $random[$values] = $result[$values];
  }
  
  return $random;
  
}

function getBooksAuth($dbh, $num_books)
{
  $query = 'SELECT 
  book.title AS title,
  author.name AS author,
  
  book.book_id AS book_id,
  genre.name AS genre,
  book.num_pages AS num,
  
  book.author_id AS author_id,
  book.year_published AS year,
  book.price AS price,
  book.image AS image,
  book.description AS description
  FROM book
  JOIN author USING(author_id)
  JOIN genre USING(genre_id)
  WHERE author_id = :author_id 
  ';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':author_id'=>$author_id);
  $stmt -> execute();
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
 
  
  
  
}



function getBookDetail($dbh, $book_id)
{
  $query = 'SELECT 
  book.title AS title,
  author.name AS author,
  book.book_id AS book_id,
  genre.name AS genre,
  book.num_pages AS num,
  book.author_id AS author_id,
  book.year_published AS year,
  book.in_print AS in_print,
  book.price AS price,
  book.image AS image,
  publisher.name AS publisher,
  publisher.city AS pub_city,
  author.country AS country,
  format.name AS format,
  publisher.publisher_id AS publisher_id,
  book.description AS description
  FROM book
  JOIN author USING(author_id)
  JOIN publisher USING(publisher_id)
  JOIN genre USING(genre_id)
  JOIN format USING(format_id)
  WHERE book_id=:book_id';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':book_id'=>$book_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetch(PDO::FETCH_ASSOC);
 
  
}

function getBooksPub($dbh, $publisher_id)
{
  $query = 'SELECT 
  
  book.image AS image
  FROM book
  
  JOIN publisher USING(publisher_id)
  
  WHERE publisher_id=:publisher_id';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':publisher_id'=>$publisher_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
 
  
}

function searchBooks($dbh, $keyword)
{
  $query = "SELECT 
  book.title AS title,
  author.name AS author,
  
  book.book_id AS book_id,
  genre.name AS genre,
  book.num_pages AS num,
  
  book.author_id AS author_id,
  book.year_published AS year,
  book.price AS price,
  book.image AS image,
  book.description AS description
  FROM book
  JOIN author USING(author_id)
  JOIN genre USING(genre_id)
   WHERE MATCH (title)
        AGAINST (:keyword IN NATURAL LANGUAGE MODE)
  ";
  
  $stmt =$dbh-> prepare($query);
  $stmt->bindValue(':keyword', $keyword, PDO::PARAM_INT);
  $stmt -> execute();
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
 
  
  
  
}
