<?php

//config
/**
      * In class exercise
      * @file config.php
      * @course Advanced PHP, WDD 2018 Jan
      * @author Navdeep dhindsa <dhindsanavdeep24@gmail.com>
      * @created_at 2018-08-24
   */


ini_set('display_errors',1);
ini_set('error_reporting', E_ALL);

require '../lib/book.php';
require '../lib/genre.php';

define('DB_USER','root');
define('DB_PASS','root');
define('DB_DSN','mysql:host=localhost;dbname=booksite');

$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);



