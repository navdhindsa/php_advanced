<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Bookstore</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />

	<style>

		.book_cover {
			width: 25%;
			float: left;
			clear: both;
		}

		.book_cover img {
			width: auto;
			height: auto;
			max-width: 100%;
		}

		.book_details {
			width: 50%;
			float: left;
			padding: 0 20px;
		}

		.book_author {
			width: 25%;
			float: left;
			font-size: 80%;
		}

		.book_author img {
			width: auto;
			height: auto;
			max-width: 100%;
		}

		.book_description {

			padding: 30px;
			border: 1px solid #cfcfcf;
			margin-bottom: 30px;
			clear: both;
		}



		.book_item {
			width: 15%;
			float: left;
			margin-right: 10px;
			padding: 20px;
			border: 1px solid #cfcfcf;
		}

		.book_item img {
			width: auto;
			height: auto;
			max-width: 100%;
		}

	</style>
</head>
<body>

<div class="container">

	<div id="header">

	<nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="index.html">home</a></li><li>
			<a href="books.html">books</a></li><li>
			<a href="about.html">about</a></li><li>
			<a href="contact.html">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->

	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form>
				<input type="text" name="s" maxlength="255" />&nbsp;
				<input type="submit" value="search" />
			</form>
		</div>

		<hr class="clear" />

		<h1>Detail</h1>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
			<li><a href="">Mystery</a></li>
			<li><a href="">Horror</a></li>
			<li><a href="">Romance</a></li>
			<li><a href="">Literature</a></li>
			<li><a href="">Drama</a></li>
		</ul>

	</div>

	<div class="shelf">

		<div class="book_cover">

			<img src="images/covers/under_the_dome.jpg" alt="Under the Dome" />

		</div><!-- /.book_cover -->

		<div class="book_details">

			<h3>Under the Dome</h3>

			<ul>
				<li><strong>Title</strong>: Under the Dome</li>
				<li><strong>Author</strong>: Stephen King</li>
				<li><strong>Genre</strong>: Horror</li>
				<li><strong>Format</strong>: Trade Paper</li>
				<li><strong>Number of Pages</strong>: 1200</li>
				<li><strong>Year Published</strong>: 2010</li>
				<li><strong>In Print</strong>: Yes</li>
				<li><strong>Price</strong>: $22.99</li>
				<li><strong>Publisher</strong>: Scribners</li>
				<li><strong>Publisher City</strong>: New York</li>
			</ul>

		</div><!-- /.book_details -->

		<div class="book_author">

			<h4>Meet the author...</h4>

			<h5>Stephen King</h5>

			<img src="images/authors/stephen_king.jpg" alt="Stephen King" />

			<p>Stephen King.  Country: USA</p>

			<img src="images/countries/usa.jpg" alt="USA Flag" />

			<p>View <a href="">other books by this author</a>.</p>

		</div><!-- /.book_author -->

		<div class="book_description">
						<h4>Description</h4>

			<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>

		</div>

		<div class="book_publisher">

			<h3>Other books by this publisher</h3>

			<div class="book_item">
				<a href=""><img src="images/covers/the_oath.jpg" alt="The Oath" /></a>
			</div>

			<div class="book_item">
				<a href=""><img src="images/covers/carrie.jpg" alt="Carrie" /></a>
			</div>

			<div class="book_item">
				<a href=""><img src="images/covers/not_a_penny_more.jpg" alt="Not a Penny More" /></a>
			</div>

		</div>

		

	</div><!-- /.shelf -->

</div<!-- /.container -->

</body>
</html>