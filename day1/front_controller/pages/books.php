<?php


$genres= getGenres($dbh);
$books = getBooks($dbh,5);



?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Bookstore</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
</head>
<body>

<div class="container">

	<div id="header">

<?php include __DIR__.  '/../inc/nav.inc.php'; ?>

	</div><!-- /#header -->

	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form>
				<input type="text" name="s" maxlength="255" />&nbsp;
				<input type="submit" value="search" />
			</form>
		</div>

		<hr class="clear" />

		<h1>Books</h1>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
			<?php foreach($genres AS $row) : ?>
         
			<li><a href="books.php?genre_id=<?=$row['genre_id']?>"><?=$row['name']?></a></li>
		
          <?php endforeach;?>
			
		</ul>

	</div>

	<div class="shelf">

		<div class="book">

			<div class="img">
				<img src="images/covers/under_the_dome.jpg" alt="Under The Dome" />
			</div>
			<div class="details">
				<p><strong>Under the Dome</strong><br />
					by <a href="">Stephen King</a><br />
					<span>Horror</span>, 565 pages, 2011, $18.99</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium felis egestas massa pulvinar varius. Phasellus ut diam sit amet justo mattis iaculis.</p>
					<p><a class=more" href="">More info</a>
			</div>

		</div><!-- /.book -->

		<div class="book">

			<div class="img">
				<img src="images/covers/the_oath.jpg" alt="The Oath" />
			</div>
			<div class="details">
				<p><strong>The Oath</strong><br />
					by <a href="">John Lescroart</a><br />
					<span>Mystery</span>, 565 pages, 2014, $22.99</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium felis egestas massa pulvinar varius. Phasellus ut diam sit amet justo mattis iaculis.</p>
					<p><a class=more" href="">More info</a>
			</div>

		</div><!-- /.book -->

		<div class="book">

			<div class="img">
				<img src="images/covers/caves_of_steel.jpg" alt="Caves of Steel" />
			</div>
			<div class="details">
				<p><strong>Caves of Steel</strong><br />
					by <a href="">Isaac Asimove</a><br />
					<span>SF</span>, 265 pages, 1958, $7.99</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium felis egestas massa pulvinar varius. Phasellus ut diam sit amet justo mattis iaculis.</p>
					<p><a class=more" href="">More info</a>
			</div>

		</div><!-- /.book -->

		<div class="book">

			<div class="img">
				<img src="images/covers/not_a_penny_more.jpg" alt="Not a Penny More" />
			</div>
			<div class="details">
				<p><strong>Not a Penny More</strong><br />
					by <a href="">Jeffrey Archer</a><br />
					<span>Drama</span>, 323 pages, 2987, $16.99</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium felis egestas massa pulvinar varius. Phasellus ut diam sit amet justo mattis iaculis.</p>
					<p><a class=more" href="">More info</a>
			</div>

		</div><!-- /.book -->

		<div class="book">

			<div class="img">
				<img src="images/covers/island.jpg" alt="Island" />
			</div>
			<div class="details">
				<p><strong>Island</strong><br />
					by <a href="">Peter Benchley</a><br />
					<span>Literature</span>, 454 pages, 1974, $12.99</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus pretium felis egestas massa pulvinar varius. Phasellus ut diam sit amet justo mattis iaculis.</p>
					<p><a class=more" href="">More info</a>
			</div>

		</div><!-- /.book -->

	</div><!-- /.shelf -->

</div<!-- /.container -->

</body>
</html>