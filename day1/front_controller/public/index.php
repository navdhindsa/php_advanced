<?php
ob_start();
session_start();

//base path(current directory)
define('APP', __DIR__);
require APP.'/../config.php';

$allowed =array('about','books','index','contact','detail','login','register');

if(in_array($_GET['page'], $allowed)){
	
	include '../pages/'.$_GET['page'].'.php';
}
elseif(empty($_GET['page'])){
	include '../pages/index.php';
}
else{
	header('HTTP/1.0 404 NOT FOUND');
	include '../pages/error404.php';
}
?>