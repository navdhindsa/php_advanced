<?php

require APP. '/models/book.php';

define('PST',.08);
define('GST',.05);

function addToCart($dbh,$book_id)
{
  $book=getBookDetail($dbh,$book_id);
  $cart = [
    'title' =>$book['title'],
    'price'=>$book['price'],
    'author'=>$book['author']
  ];
  $_SESSION['cart']=$cart;
  
}