<?php


include __DIR__ . '/inc/header.inc.php';

?>








		<h1><?=$title?></h1>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
			<?php foreach($genres AS $row) : ?>
         
			<li><a href="?page=books&genre_id=<?=$row['genre_id']?>"><?=$row['name']?></a></li>
		
          <?php endforeach;?>
		</ul>

	</div>

<?php if(!empty($_SESSION['cart'])) {
  include APP . '/models/cart_include.php';
} ?>
	<div class="shelf">

		<div class="book_cover">

			<img src="images/covers/<?=$books['image']?>" alt="Under the Dome" />

		</div><!-- /.book_cover -->

		<div class="book_details">

			<h3><?=$books['title']?></h3>

			<ul>
				<li><strong>Title</strong>: <?=$books['title']?></li>
				<li><strong>Author</strong>: <?=$books['author']?></li>
				<li><strong>Genre</strong>: <?=$books['genre']?></li>
				<li><strong>Format</strong>: <?=$books['format']?></li>
				<li><strong>Number of Pages</strong>: <?=$books['num']?></li>
				<li><strong>Year Published</strong>: <?=$books['year']?></li>
				<li><strong>In Print</strong>: <?=$books['in_print']?></li>
				<li><strong>Price</strong>: $<?=$books['price']?></li>
				<li><strong>Publisher</strong>: <?=$books['publisher']?></li>
				<li><strong>Publisher City</strong>: <?=$books['pub_city']?></li>
                <li><form action="?page=cart" method="post">
                      <input type="hidden" name="book_id" value="<?=$books['book_id']?>" />
                  <button type="submit">Add to cart</button>
                  
                  
                  
                  </form></li>
			</ul>

		</div><!-- /.book_details -->

		<div class="book_author">

			<h4>Meet the author...</h4>

			<h5><?=$books['author']?></h5>
             <?php $img=  str_replace(" ", "_",strtolower($books['author']));?>
			<img src="images/authors/<?=$img?>.jpg" alt="<?=$books['author']?>" />

			<p><?=$books['author']?>.  Country: <?=$books['country']?></p>
    
			<img src="images/countries/<?=strtolower($books['country'])?>.jpg" alt="USA Flag" />

			<p>View <a href="books.php?author_id=<?=$author_id?>">other books by this author</a>.</p>

		</div><!-- /.book_author -->

		<div class="book_description">
						<h4>Description</h4>

			<p><?=$books['description']?></p>

		</div>

		<div class="book_publisher">

			<h3>Other books by this publisher</h3>
            <?php foreach($pub_books AS $row) : ?>
          
			<div class="book_item">
              
				<a href=""><img src="images/covers/<?=$row['image']?>" alt="The Oath" /></a>
			</div>

			<?php endforeach; ?>

		</div>



	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php
include __DIR__ . '/inc/footer.inc.php';
?>