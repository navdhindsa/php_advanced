<?php


include __DIR__ . '/inc/header.inc.php';
?>



		<h1><?=$title?></h1>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
          <?php foreach($genres AS $row) : ?>
         
			<li><a href="?page=books&genre_id=<?=$row['genre_id']?>"><?=$row['name']?></a></li>
		
          <?php endforeach;?>
		</ul>

	</div>
<?php if(!empty($_SESSION['cart'])) {
  include APP . '/models/cart_include.php';
} ?>
	<div class="shelf">
      <h3>
      <?php if(!empty($_GET['genre_id'])) : ?>
      
       
        
       <?=$books[0]['genre']?>
      
     <?php endif; ?>
        
        <?php if(!empty($_GET['author_id'])) : ?>
      
       
        
       <?=$books[0]['author']?>
      
     <?php endif; ?>
       
      
      </h3>
      
<?php foreach($books AS $row) : ?>
		<div class="book">

			<div class="img">
				<img src="images/covers/<?=$row['image']?>" alt="<?=$row['title']?>" />
			</div>
			<div class="details">
				<p><strong><?=$row['title']?></strong><br />
					by <a href="?page=books&author_id=<?=$row['author_id']?>"><?=$row['author']?></a><br />
					<span><?=$row['genre']?></span>, <?=$row['num']?> pages, <?=$row['year']?>, $<?=$row['price']?></p>
					<p><?=$row['description']?></p>
					<p><a class="more" href="?page=detail&book_id=<?=$row['book_id']?>">More info</a>
			</div>

		</div><!-- /.book -->

		<?php endforeach; ?>

	 <?php if(empty($books)) : ?>
      
       
        
       <p>Sorry, no books found !!</p>
      
     <?php endif; ?>

	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php
include __DIR__ . '/inc/footer.inc.php';
?>