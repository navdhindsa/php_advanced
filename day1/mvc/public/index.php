<?php

ob_start();
session_start();

define('APP',__DIR__.'/..');

require APP. '/config.php';


$allowed =array('about','books','cart','contact','detail','home','login','register');

if(in_array($_GET['page'], $allowed)){
	
	include APP . '/controllers/'.$_GET['page'].'.php';

}
elseif(empty($_GET['page'])){
	include APP . '/home.php';
}
else{
	header('HTTP/1.0 404 NOT FOUND');
	include APP . '/error404.php';
}
?>