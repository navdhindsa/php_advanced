<?php 

$title="Contact" ;
$slug="contact";



require APP.'/views/inc/functions.php';


if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $errors = [];
  $flag=0;
  
  
  
if (isset($_POST['phone'])){
  
  if(empty($_POST['phone'])) {
        $errors['phone'] = "Phone is a required field."; 
    }
  
  
}

//    $errors = validateEmail('email', $errors);

 // $errors = passwordsMatch('password', 'confirm_password', $errors);
 
  
  if(!filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)) {
        $errors2 = "Please enter a valid email address";
        $flag=1;
    }
  
  $errors=required();

  if(!is_numeric($_POST['age'])){
    $errors3= "Age is not a number";
    $flag =1;
  }
  if(is_numeric($_POST['age']) && $_POST['age']>110){
    $errors4= "We don't think you are that old!";
    $flag =1;
  }
  if(is_numeric($_POST['age']) && $_POST['age']<6){
    $errors5= "Please don't be so modest!";
    $flag =1;
  }
   
  if(!is_numeric($_POST['phone'])){
    $errors6= "Phone is not a number";
    $flag =1;
  }
  if(is_numeric($_POST['phone']) && strlen($_POST['phone'])!=10){
    $errors7= "Phone number should be 10 digits long!";
    $flag =1;
  }
  
    if(empty($errors )&& $flag!=1) {

        $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = 'INSERT INTO users 
                (password, age, first_name, last_name, email, comment, phone)
                VALUES
                (:password, :age,:first_name, :last_name, :email,  :comment,  :phone)';

        $stmt = $dbh->prepare($query);

        $params = array (
            ':first_name' => $_POST['first_name'],
            ':last_name' => $_POST['last_name'],
            ':email' => $_POST['email'],
           ':phone' => $_POST['phone'],
            
            ':comment'=>$_POST['comment'], 
          ':age'=>$_POST['age'],
          ':password'=>$_POST['password']
          
            
        );


        if($stmt->execute($params)) {

            $id = $dbh->lastInsertId();

            $query = 'SELECT * FROM users
                    WHERE id = :id';
            $stmt = $dbh->prepare($query);

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            $users = $stmt->fetch(PDO::FETCH_ASSOC);

            $success = true;

        } // end if stmt 

    } // end if no errors
    
} // end if POST submission


include APP. '/views/contact.php';














