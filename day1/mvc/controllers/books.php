<?php 

$title="Books" ;
$slug="books";

include APP . '/models/genre.php';
include APP . '/models/book.php';
include APP . '/models/author.php';



$genres= getGenres($dbh);
$books = getBooks($dbh,5);
if(!empty($_GET['genre_id'])){
  
  $genre_id=$_GET['genre_id'];
  
  $books = getGenre($dbh, $genre_id);
}
if(!empty($_GET['author_id'])){
  
  $author_id=$_GET['author_id'];
  
  $books = getAuthor($dbh, $author_id);
}




include APP. '/views/books.php';