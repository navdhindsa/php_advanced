<?php





if(!empty($_GET['s'])){
  
  $keyword=$_GET['s'];
  $books = searchBooks($dbh, $keyword);
 
}





?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?=$title?></title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
  
  
  <?php if($slug=='detail') : ?>
  	<style>

		.book_cover {
			width: 25%;
			float: left;
			clear: both;
		}

		.book_cover img {
			width: auto;
			height: auto;
			max-width: 100%;
		}

		.book_details {
			width: 50%;
			float: left;
			padding: 0 20px;
		}

		.book_author {
			width: 25%;
			float: left;
			font-size: 80%;
		}

		.book_author img {
			width: auto;
			height: auto;
			max-width: 100%;
		}

		.book_description {

			padding: 30px;
			border: 1px solid #cfcfcf;
			margin-bottom: 30px;
			clear: both;
		}



		.book_item {
			width: 15%;
			float: left;
			margin-right: 10px;
			padding: 20px;
			border: 1px solid #cfcfcf;
		}

		.book_item img {
			width: auto;
			height: auto;
			max-width: 100%;
		}

	</style>
  
  
  <?php endif; ?>
  

  
</head>
<body>

<div class="container">

	<div id="header">
      
    <nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="index.php">home</a></li><li>
			<a href="books.php">books</a></li><li>
			<a href="about.php">about</a></li><li>
			<a href="contact.php">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->
  
  <?php if($slug!='index') : ?>
  
  
	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form>
				<input type="text" name="s" maxlength="255" />&nbsp;
				<input type="submit" value="search" />
			</form>
		</div>

		<hr class="clear" />
  
  <?php endif; ?>