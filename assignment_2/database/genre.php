<?php


//category


function getGenres($dbh)
{
  $query = 'SELECT distinct genre.name AS name , genre.genre_id As genre_id FROM genre join book using(genre_id) where title is not null';
  
  $stmt =$dbh-> prepare($query);
  
  $stmt -> execute();
  
  return $stmt-> fetchAll(PDO::FETCH_ASSOC);
}

function getGenre($dbh, $genre_id)
{
  $query = 'SELECT 
  book.title AS title,
  author.name AS author,
  genre.name AS genre,
  
  book.book_id AS book_id,
  book.author_id AS author_id,
  book.num_pages AS num,
  book.year_published AS year,
  book.price AS price,
  book.image AS image,
  book.description AS description
  FROM book
  JOIN author USING(author_id)
  JOIN genre USING(genre_id)
  WHERE genre_id=:genre_id';
  
  $stmt =$dbh-> prepare($query);
  $params= array(':genre_id'=>$genre_id);
  $stmt -> execute($params);
  
  return  $stmt-> fetchAll(PDO::FETCH_ASSOC);
 
  
  
  
}