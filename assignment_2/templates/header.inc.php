<?php











?><!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title><?=$title?></title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
  
    <?php include 'headerstyle.inc.php'; ?>

    <script>
      $(document).ready(function(){
      	$('#search_form input').keyup(function(e){

      		var keyword = {};
      		keyword.string = $('form #s').val();

      		$.post('../database/server.php', keyword, function(response){
					
					viewResults(response);
				});
      		
      	});
      	
      });

      function viewResults(response){
      	
      	var html = '<ul>';
      	//console.log(response[0].title);
      	for(var i=0; i<response.length; i++){
      		// html += '<li data-id="'+response[i].book_id+'">'+response[i].title+'</li>';
      		 html+='<li ><a  href="detail.php?book_id='+response[i].book_id+'">'+response[i].title+'</a></li>';

      	}

      	html +='</ul>';
      	$('#search_box').html(html);
      }

    </script>
</head>
<body>

<div class="container">

	<div id="header">
      
    <nav>
		<img id="logo" src="images/logo.jpg" alt="logo" />
		<ul>
			<li class="current"><a href="index.php">home</a></li><li>
			<a href="books.php">books</a></li><li>
			<a href="about.php">about</a></li><li>
			<a href="contact.php">contact</a></li>
		</ul>

	</nav>

	</div><!-- /#header -->
  
  <?php if($slug!='index') : ?>
  
  
	<div class="header_img">
		<img src="images/header.jpg" />
	</div>

			<div class="search">

			<form id="search_form" autocomplete="off" novalidate action="" method="">
				<input id="s" type="text" name="s" maxlength="255" />&nbsp;<input type="submit" value="search" />
				<div id="search_box"></div>
			</form>

		</div>
        
		<hr class="clear" />
  
  <?php endif; ?>