<?php 

$title="Contact" ;
$slug="contact";

include __DIR__ . '/../config/config.php'; 


require '../templates/functions.php';


if($_SERVER['REQUEST_METHOD'] == 'POST') {

    $errors = [];
  $flag=0;
  
  
  
if (isset($_POST['phone'])){
  
  if(empty($_POST['phone'])) {
        $errors['phone'] = "Phone is a required field."; 
    }
  
  
}

//    $errors = validateEmail('email', $errors);

 // $errors = passwordsMatch('password', 'confirm_password', $errors);
 
  
  if(!filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL)) {
        $errors2 = "Please enter a valid email address";
        $flag=1;
    }
  
  $errors=required();

  if(!is_numeric($_POST['age'])){
    $errors3= "Age is not a number";
    $flag =1;
  }
  if(is_numeric($_POST['age']) && $_POST['age']>110){
    $errors4= "We don't think you are that old!";
    $flag =1;
  }
  if(is_numeric($_POST['age']) && $_POST['age']<6){
    $errors5= "Please don't be so modest!";
    $flag =1;
  }
   
  if(!is_numeric($_POST['phone'])){
    $errors6= "Phone is not a number";
    $flag =1;
  }
  if(is_numeric($_POST['phone']) && strlen($_POST['phone'])!=10){
    $errors7= "Phone number should be 10 digits long!";
    $flag =1;
  }
  
    if(empty($errors )&& $flag!=1) {

        $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $query = 'INSERT INTO users 
                (password, age, first_name, last_name, email, comment, phone)
                VALUES
                (:password, :age,:first_name, :last_name, :email,  :comment,  :phone)';

        $stmt = $dbh->prepare($query);

        $params = array (
            ':first_name' => $_POST['first_name'],
            ':last_name' => $_POST['last_name'],
            ':email' => $_POST['email'],
           ':phone' => $_POST['phone'],
            
            ':comment'=>$_POST['comment'], 
          ':age'=>$_POST['age'],
          ':password'=>$_POST['password']
          
            
        );


        if($stmt->execute($params)) {

            $id = $dbh->lastInsertId();

            $query = 'SELECT * FROM users
                    WHERE id = :id';
            $stmt = $dbh->prepare($query);

            $stmt->bindValue(':id', $id, PDO::PARAM_INT);

            $stmt->execute();

            $users = $stmt->fetch(PDO::FETCH_ASSOC);

            $success = true;

        } // end if stmt 

    } // end if no errors
    
} // end if POST submission



















include __DIR__ . '/../templates/header.inc.php';





?>
	
<?php if(!empty($_SESSION['cart'])) {
  include '../database/cart_include.php';
} ?>

		<h1><?=$title?></h1>
<?php if(empty($success)) : ?>
		<h1>Please fill the form</h1>

<form action="contact.php" method="post" accept-charset="utf-8" novalidate>

<fieldset>
  <legend>The contact form</legend>
  
  <p>
              <label for="first_name">First Name </label>
              <input type="text"
                id="first_name" 
                name="first_name" 
                maxlength="25"
                size="30"
                placeholder="First Name" value="<?php if(!empty($_POST['first_name'])) 
                    echo $_POST['first_name']; ?>"
                />
            </p>
            <?php  if(!empty($errors['first_name'])) : ?>
            <span class="error"><?=$errors['first_name']?></span>
             <?php endif; ?>
  <p>
  <label for="last_name">Last Name </label>
              <input type="text"
                id="last_name" 
                name="last_name" 
                maxlength="25"
                size="30"
                placeholder="Last Name" value="<?php if(!empty($_POST['last_name'])) 
                    echo $_POST['last_name']; ?>"
                />
            </p>
  
  <?php if(!empty($errors['last_name'])) : ?>
            <span class="error"><?=$errors['last_name']?></span>
             <?php endif; ?>
  <p>
              <label for="email">Email Address </label> 
              <input type="email" 
                     name="email" 
                     id="email" placeholder="Email" value="<?php if(!empty($_POST['email'])) 
                    echo $_POST['email']; ?>"
                     />
            </p>
  <?php if(!empty($errors['email'])) : ?>
            <span class="error"><?=$errors['email']?></span>
             <?php endif; ?>
            <?php if(!empty($errors2)) : ?>
            <span class="error"><?=$errors2?></span>
             <?php endif; ?>
  
  
  <p >
            <label  for="age">Age</label>
            <input type="text" name="age" maxlength="255"
            value="<?php if(!empty($_POST['age'])) 
                    echo esc_attr($_POST['age']); ?>" />
            
        </p>
            <?php if(!empty($errors['age'])) : ?>
            <span class="error"><?=$errors['age']?></span>
             <?php endif; ?>
            <?php if(!empty($errors3)) : ?>
            <span class="error"><?=$errors3?></span>
             <?php endif; ?>
            <?php if(!empty($errors4)) : ?>
            <span class="error"><?=$errors4?></span>
             <?php endif; ?>
            <?php if(!empty($errors5)) : ?>
            <span class="error"><?=$errors5?></span>
             <?php endif; ?>
            
  <p>
            <label for="phone">Telephone </label>
              <input type="text"
                     id="phone"
                     name="phone"
                     maxlength="18"placeholder="Phone" value="<?php if(!empty($_POST['phone'])) 
                    echo $_POST['phone']; ?>"
                      />
            </p>
  <?php if(!empty($errors['phone'])) : ?>
            <span class="error"><?=$errors['phone']?></span>
             <?php endif; ?>
   <?php if(!empty($errors6)) : ?>
            <span class="error"><?=$errors6?></span>
             <?php endif; ?>
            <?php if(!empty($errors7)) : ?>
            <span class="error"><?=$errors7?></span>
             <?php endif; ?>
  <p>
    <p>
              <label for="password">Password</label>
              <input type="password"
                id="password" 
                name="password" 
                maxlength="25"
                size="30"
                placeholder="Password" 
                />
            </p>
            <?php if(!empty($errors['password'])) : ?>
            <span class="error"><?=$errors['password']?></span>
             <?php endif; ?>
        <?php if(!empty($errors1)) : ?>
            <span class="error"><?=$errors1?></span>
             <?php endif; ?>
            <p>
              <label for="confirm_password">Confirm Password</label>
              <input type="password"
                id="confirm_password" 
                name="confirm_password" 
                maxlength="25"
                size="30"
                placeholder="Confirm Password"
                />
            </p>
            <?php if(!empty($errors['confirm_password'])) : ?>
            <span class="error"><?=$errors['confirm_password']?></span>
             <?php endif; ?>
  <input type="checkbox" name="phone" value="phone">Contact me by phone</input>
  
  
  
  </p>
  <p>
              <label for="comment">Please leave your comments</label>  <br />
              <textarea cols="30" 
                        rows="6" 
                        id="comment" 
                        name="comment" value="<?php if(!empty($_POST['comment'])) 
                    echo esc_attr($_POST['comment']); ?>">
              </textarea>
            </p>
   <p>
              <input type="submit" 
                     name="submit" 
                     id="submit" 
                     value="Submit" 
              />
              &nbsp;&nbsp;
              <input type="reset" 
                     name="reset" 
                     id="reset" 
                     value="Reset" 
              />
              </p>
  
  </fieldset>



</form>

 <?php else : ?>

    <h2>Thankyou for registering!</h2>

    

    <p>Back to <a href="connect.php">Registration Form</a></p>

  
    
<?php endif; ?>

</div<!-- /.container -->

<?php
include __DIR__ . '/../templates/footer.inc.php';
?>