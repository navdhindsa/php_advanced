<?php 

$title="Books" ;
$slug="books";

include __DIR__ . '/../config/config.php'; 
include __DIR__ . '/../database/genre.php';
include __DIR__ . '/../database/book.php';
include __DIR__ . '/../database/author.php';



$genres= getGenres($dbh);
$books = getBooks($dbh,5);
if(!empty($_GET['genre_id'])){
  
  $genre_id=$_GET['genre_id'];
  
  $books = getGenre($dbh, $genre_id);
}
if(!empty($_GET['author_id'])){
  
  $author_id=$_GET['author_id'];
  
  $books = getAuthor($dbh, $author_id);
}

if(!empty($_GET['s'])){
   
  $keyword=$_GET['s'];
  $books = searchBooks($dbh, $keyword);
  
}






include __DIR__ . '/../templates/header.inc.php';
?>



		<h1><?=$title?></h1>

	<div class="categories">

		<h3>Categories</h3>

		<ul>
          <?php foreach($genres AS $row) : ?>
         
			<li><a href="books.php?genre_id=<?=$row['genre_id']?>"><?=$row['name']?></a></li>
		
          <?php endforeach;?>
		</ul>

	</div>
<?php if(!empty($_SESSION['cart'])) {
  include '../database/cart_include.php';
} 
?>
	<div class="shelf">
      <h3>
      <?php if(!empty($_GET['genre_id'])) : ?>
      
       
        
       <?=$books[0]['genre']?>
      
     <?php endif; ?>
        
        <?php if(!empty($_GET['author_id'])) : ?>
      
       
        
       <?=$books[0]['author']?>
      
     <?php endif; ?>
       
      
      </h3>
      
<?php foreach($books AS $row) : ?>
		<div class="book">

			<div class="img">
				<img src="images/covers/<?=$row['image']?>" alt="<?=$row['title']?>" />
			</div>
			<div class="details">
				<p><strong><?=$row['title']?></strong><br />
					by <a href="books.php?author_id=<?=$row['author_id']?>"><?=$row['author']?></a><br />
					<span><?=$row['genre']?></span>, <?=$row['num']?> pages, <?=$row['year']?>, $<?=$row['price']?></p>
					<p><?=$row['description']?></p>
					<p><a class="more" href="detail.php?book_id=<?=$row['book_id']?>">More info</a>
			</div>

		</div><!-- /.book -->

		<?php endforeach; ?>

	 <?php if(empty($books)) : ?>
      
       
        
       <p>Sorry, no books found !!</p>
      
     <?php endif; ?>

	</div><!-- /.shelf -->

</div<!-- /.container -->

<?php
include __DIR__ . '/../templates/footer.inc.php';
?>